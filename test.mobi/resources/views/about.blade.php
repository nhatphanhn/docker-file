@extends('layouts.basic')

@section('content')
<div class="content">
    <div class="mb-16">
        ToDo List · API is a Laravel app for ToDo List project.
    </div>
    <div class="grid grid-cols-3 gap-4">
        @foreach ($repoLinks as $repoLink)
            <div class="bg-white hover:bg-gray-200  transition duration-300">
                <a href="{{ $repoLink['link'] }}">
                    <div class="max-w-sm rounded overflow-hidden shadow-lg">
                        <div class="px-6 py-4">
                            <span class="font-dancing-script-regular text-2xl pb-5 block">
                                {{ $repoLink['label'] }}    
                            </span>        
                            <div>
                                {{ $repoLink['infos'] }}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>
@endsection
