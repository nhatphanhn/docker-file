module.exports = {
  theme: {
    extend: {
      colors: {
        purple: '#84256C',
        'gray-c': '#c3c3c3',
        lightwhite: '#FAFAFA'
      },
      fontSize: {
        '7xl': '12rem'
      }
    },
    alphaValues: [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
    alphaColors: ['purple', 'white', 'gray-c']
  },
  plugins: [require('tailwindcss-bg-alpha')()]
}
