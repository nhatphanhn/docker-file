@extends('layouts.basic')

@section('content')
<div class="content">
    <div>
        @foreach ($routes as $route)
            <a href="{{ $route['url'] }}">
                {{ $route['url'] }}
            </a>
        @endforeach
    </div>
</div>
@endsection
