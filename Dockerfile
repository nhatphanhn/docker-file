FROM  webdevops/php-nginx
WORKDIR /var/www/html/
ADD test.mobi.conf /etc/nginx/conf.d/test.mobi.conf
COPY . .
RUN cd /var/www/html/test.mobi && composer install --optimize-autoloader
