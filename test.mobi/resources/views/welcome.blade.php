@extends('layouts.basic')

@section('content')
<div class="content">
    <div class="flex">
        <img
            src="{{ asset('images/logo.png') }}"
            alt="logo"
            class="mx-auto"
        />
    </div>
    <div class="title m-b-md font-dancing-script-regular">
        {{ config('app.name') }}
    </div>
    <div class="links font-dancing-script-regular">
        <a href="{{ route('api-infos') }}">
            API
        </a>
        <a href="{{ route('about') }}">
            About
        </a>
    </div>
</div>
@endsection
