<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'NavigationController@welcome')->name('welcome');
Route::get('/api-infos', 'NavigationController@apiInfos')->name('api-infos');
Route::get('/about', 'NavigationController@about')->name('about');
Route::get('/home', 'HomeController@index')->name('home');
