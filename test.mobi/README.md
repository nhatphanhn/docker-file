# **ToDo List · Back**

[![Laravel 5.7](https://img.shields.io/static/v1?label=Laravel&message=v5.7&color=ff2d20&style=flat-square&logo=laravel&logoColor=ff2d20)](https://laravel.com)
[![PHP 7.1](https://img.shields.io/static/v1?label=PHP&message=v7.1&color=777bb4&style=flat-square&logo=php&logoColor=777bb4)](https://www.php.net)
[![NodeJS 11.15](https://img.shields.io/static/v1?label=NodeJS&message=v11.15&color=339933&style=flat-square&logo=node.js&logoColor=339933)](https://nodejs.org/en)
[![Composer 1.8](https://img.shields.io/static/v1?label=Composer&message=v1.8&color=885630&style=flat-square&logo=composer&logoColor=9f7759)](https://getcomposer.org)
[![Yarn 1.22](https://img.shields.io/static/v1?label=Yarn&message=v1.22&color=2C8EBB&style=flat-square&logo=yarn&logoColor=2C8EBB)](https://classic.yarnpkg.com/lang/en/)

## **Front**

[![TodoList · Front repository 1.1](https://img.shields.io/static/v1?label=TodoList-Front&message=v1.1&color=4FC08D&style=flat-square&logo=vue.js&logoColor=4FC08D)](https://gitlab.com/EwieFairy/todo-list-front)  
To use this project, you have to set front with [**TodoList · Front repository**](https://gitlab.com/EwieFairy/todo-list-front).

## **1. Initialization**

Create **`todo-list`** database into phpMyAdmin or with CLI.

```bash
# create .env
cp .env.example .env
```

Replace these informations:  
> DB_DATABASE=todo-list  
> DB_USERNAME=`<username>`  
> DB_PASSWORD=`<password>`  

```bash
# download dependencies
composer install
# generate key
php artisan key:generate
# migrate database
php artisan migrate:fresh --seed
# nodejs dependencies
yarn
# assets
yarn dev
```

## **2. VHost**

### ***A. NGINX***

```nginx
server {
    listen 80;
    root /var/www/todo-list-back/public;
    index index.php index.html index.htm index.nginx-debian.html;
    server_name todo-list.localhost;
    
    error_log /var/log/todo-list-back.log warn;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location /.* {
        root /var/www/todo-list-back/storage/app/public;
        autoindex on;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}
```

Now, you can access to your application to [**http://todo-list.localhost**](http://todo-list.localhost). If you don't wan to set VHost, you can access to your app with `php artisan serve`.

## **3. API Routes**

- **List of user tasks**  
*GET method*  
```bash
/api/tasks?api_token=YOURAPITOKEN
```

- **Add a task for the user**  
*POST method*  
```bash
/api/tasks/add?api_token=YOURAPITOKEN
```

- **Edit the task _id for the user**  
*PUT method*  
```bash
/api/tasks/_id/update?api_token=YOURAPITOKEN
```

- **Delete the task _id**  
*DELETE method*  
```bash
/api/tasks/_id/delete?api_token=YOURAPITOKEN
```

- **api_token**  
*POST method*  
```bash
/api/register?name=YOURNAME&email=YOUREMAIL&password=YOURPASSWORD&c_password=YOURPASSWORD
```

- **api_token**  
*POST method*  
```bash
/api/login?email=YOUREMAIL&password=YOURPASSWORD
```

## **4. Debian with MariaDB**

If you use Debian, or MariaDB engine for your database, you have to update `app/Providers/AppServiceProvider.php` to add some infos:

```
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
```
