<?php

use Illuminate\Database\Seeder;
use App\Task;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = [
            [
                'title' => 'Buy banana',
                'description' => 'I need to have banana now!',
                'state' => 1,
                'user_id' => 1,
            ],
            [
                'title' => 'Got outside to play with friends',
                'description' => 'I want to play with others cats',
                'state' => 1,
                'user_id' => 2,
            ],
            [
                'title' => 'Eat strawberries',
                'description' => 'I loooove strawberries, I want to eat strawberries!',
                'state' => 0,
                'user_id' => 3,
            ],
            [
                'title' => 'Play with my cat',
                'description' => 'Kitty, kitty !',
                'state' => 1,
                'user_id' => 3,
            ],
            [
                'title' => 'Go outside',
                'description' => 'Have good time!',
                'state' => 0,
                'user_id' => 3,
            ]
        ];

        foreach ($tasks as $key => $task) {
            Task::create($task);
        }
    }
}
