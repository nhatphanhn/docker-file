<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'Auth\ApiAuthController@login');
Route::post('register', 'Auth\ApiAuthController@register');
Route::get('/users', 'UsersController@index')->name('users.index');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/tasks', 'TasksController@index')->name('tasks.index');
    Route::get('/user', 'TasksController@user')->name('tasks.user');
    Route::get('/tasks/{id}', 'TasksController@show')->name('tasks.show');
    Route::post('/tasks/add', 'TasksController@store')->name('tasks.store');
    Route::put('/tasks/{id}/update', 'TasksController@update')->name('tasks.update');
    Route::delete('/tasks/{id}/delete', 'TasksController@destroy')->name('tasks.destroy');
});
