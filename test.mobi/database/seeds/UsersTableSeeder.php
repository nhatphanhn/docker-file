<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Ewilan',
                'email' => 'ewilan@mail.com',
                'password' => Hash::make('password'),
                'api_token' => Str::random(60),
            ],
            [
                'name' => 'Thymara',
                'email' => 'thymara@mail.com',
                'password' => Hash::make('password'),
                'api_token' => Str::random(60),
            ],
            [
                'name' => 'User',
                'email' => 'user@mail.com',
                'password' => Hash::make('password'),
                'api_token' => Str::random(60),
            ],
        ];

        foreach ($users as $key => $user) {
            User::create($user);
        }
    }
}
