<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NavigationController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function welcome()
    {
        return view('welcome');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function apiInfos()
    {
        $routes = [
            [
                'url' => '/users',
                'method' => 'GET',
                'info' => 'List all users'
            ]
        ];
        $routesUp = [];
        foreach ($routes as $key => $value) {
            $value['url'] = config('app.url').'/api'.$value['url'];
            array_push($routesUp, $value);
        }
        $routes = $routesUp;
        
        return view('api-infos', compact('routes'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        $repoLinks = [
            [
                'label' => 'ToDo List · Back',
                'link' => 'https://gitlab.com/EwieFairy/todo-list-back',
                'infos' => 'Laravel app'
            ],
            [
                'label' => 'ToDo List · Front',
                'link' => 'https://gitlab.com/EwieFairy/todo-list-front',
                'infos' => 'VueJS app'
            ]
        ];
        
        return view('about', compact('repoLinks'));
    }
}
